# Python 집합: 생성, 맥세스 및 메소드

## <a name="intro"></a> 들어가며
이 포스팅에서는 고유한 요소<sup>[1](#footnote_1)</sup>의 순서가 지정되지 않은 컬렉션인 집합을 만들고 액세스하는 방법을 설명할 것이다. 또한 다양한 집합 메서드과 연산자를 사용하여 집합을 조작하고 비교하는 방법도 다룰 것이다. 이 포스팅 끝까지 다음을 학습할 것이다.

- 다양한 데이터 타입으로부터 집합 생성
- 인덱싱과 루프를 사용하여 집합 내 요소 액세스
- 메서드와 연산자를 사용하여 집합 내 요소 추가와 제거
- 집합 메서드와 연산자를 사용하여 합집합, 교집합, 차집합, 대칭 차집합 등의 연산 수행
- 부분 집합, 수퍼집합(superset), 디스조인트(disjoint) 등 집합 메서드와 연산자를 사용하여 집합을 비교
- 집합을 리스트, 튜플 및 사전과 같은 다른 데이터 타입으로 변환

이 포스팅에서는 Python과 데이터 분석에 대한 기본적인 이해를 하고 있다고 가정한다.

시작할 준비가 되었나요? 파이썬 집합으로 들어가보자!

## <a name="sec_02"></a> Python에서 집합이란?
집합은 파이썬에서 고유한 요소들의 순서가 지정되지 않은 집합을 나타내는 데이터 타입이다. 집합은 숫자, 문자열, 튜플 또는 부울과 같은 임의의 불변의 데이터 유형을 포함할 수 있다. 집합은 리스트, 사전 또는 집합 자체와 같은 불변의 데이터 타입을 포함할 수 없다. 집합은 멤버 테스트, 집합 연산, 시퀀스에서 중복 제거와 같은 연산을 수행하는 데 유용하다.

집합을 만들려면 `set()` 함수 또는 중괄호 `{}` 구문을 사용할 수 있다. `set()` 함수는 리스트, 튜플, 문자열 또는 range와 같은 반복 가능한 객체를 가져와 객체의 요소를 포함하는 새로운 집합을 반환한다. 중괄호 `{}` 구문을 사용하면 쉼표(`,`)로 구분하여 요소를 직접 지정하여 집합을 만들 수 있다. 그러나 중괄호 `{}` 구문을 사용하면 빈 사전이 생성되므로 빈 집합을 만들 수 없다. 빈 집합을 만들려면 인수가 없는 `set()` 함수를 사용해야 한다.

다음은 Python에서 집합을 만드는 예들 이다.

```python
# Create a set from a list
fruits = set(["apple", "banana", "cherry"])
print(fruits) # {'apple', 'banana', 'cherry'}

# Create a set from a tuple
colors = set(("red", "green", "blue"))
print(colors) # {'red', 'green', 'blue'}
# Create a set from a string
vowels = set("aeiou")
print(vowels) # {'a', 'e', 'i', 'o', 'u'}
# Create a set from a range
numbers = set(range(1, 6))
print(numbers) # {1, 2, 3, 4, 5}
# Create a set using curly braces
animals = {"cat", "dog", "bird"}
print(animals) # {'cat', 'dog', 'bird'}
# Create an empty set using set() function
empty_set = set()
print(empty_set) # set()
```

집합의 요소 순서는 임의적이며, 집합을 인쇄하거나 액세스할 때마다 변경될 수 있다. 또한 집합에는 고유한 요소만 포함되므로 반복 가능한 개체의 중복은 무시된다.

## <a name="sec_03"></a> Python에서 집합 생성 방법
앞 절에서 집합이란 무엇이며 집합을 만드는 방법으로 `set()` 함수나 중괄호 `{}` 구문을 사용한다는 것을 설명하였다. 이 절에서는 숫자, 문자열, 튜플, 리스트, 사전, 집합 자체와 같은 다양한 데이터 타입으로부터 집합을 만드는 방법을 설명할 것이다. 또한 컴프리헨션으로 집합을 만드는 방법도 배울 것이다. 이는 기존의 반복 가능한 객체로부터 새로운 집합을 만드는 간결한 방법이다.

문자열으로 집합을 만들려면 `set()` 함수 인수로 문자열을 사용하면 된다. 그러면 문자열의 문자를 요소로 하는 집합이 만들어진다. 예를 들어 다음과 같다.

```python
# Create a set from a string
word = set("hello")
print(word) # {'h', 'e', 'l', 'o'}
```

집합의 문자 순서는 임의적이며 인쇄하거나 집합을 액세스할 때마다 변경될 수 있다. 또한 집합에는 고유한 요소만 포함되어 있으므로 문자열의 중복되는 문자는 무시된다.

튜플로부터 집합을 만들려면 단일 튜플을 인수로 하는 `set()` 함수를 사용할 수 있다. 이렇게 하면 튜플의 요소를 요소로 하는 집합이 생성된다. 예를 들어 다음과 같다.

```python
# Create a set from a tuple
coords = set((1, 2, 3))
print(coords) # {1, 2, 3}
```

리스트에서 집합을 만들려면 하나의 리스트을 인수로 하는 `set()` 함수를 사용할 수 있다. 이렇게 하면 리스트의 원소를 원소로 하는 집합이 생성된다. 예를 들어 다음과 같다.

```python
# Create a set from a list
fruits = set(["apple", "banana", "cherry"])
print(fruits) # {'apple', 'banana', 'cherry'}
```

집합에는 고유한 요소만 포함되므로 리스트의 중복 요소는 무시된다.

사전으로부터 집합을 만들려면 단일 사전을 인수로 하는 `set()` 함수를 사용할 수 있다. 이렇게 하면 사전의 키를 요소로 하는 집합이 생성된다. 예를 들어 다음과 같다.

```python
# Create a set from a dictionary
person = set({"name": "Alice", "age": 25, "gender": "female"})
print(person) # {'name', 'age', 'gender'}
```

사전의 값은 집합에 포함되지 않고 키만 포함되어 있다는 것에 유의하라. 사전의 값으로 집합을 만들고 싶다면 사전의 `values()` 메서드를 사용하여 `set()` 함수에 전달하면 된다. 예를 들어 다음과 같다.

```python
# Create a set from the values of a dictionary
person = {"name": "Alice", "age": 25, "gender": "female"}
values = set(person.values())
print(values) # {'Alice', 25, 'female'}
```

다른 집합으로부터 집합을 만들려면 `set()` 함수를 인수로 사용하면 된다. 이렇게 하면 원래 집합과 원소가 같은 새 집합이 생성된다. 예를 들어 다음과 같다.

```python
# Create a set from another set
colors = {"red", "green", "blue"}
copy = set(colors)
print(copy) # {'red', 'green', 'blue'}
```

이것은 원래의 집합을 새로운 변수에 할당하는 집합 객체가 아니라 같은 집합 객체에 대한 참조를 생성하기 때문에 동일한 집합 객체에 할당하는 것과 동일하지 않다. 예를 들어 다음과 같다.

```python
# Assign a set to a new variable
colors = {"red", "green", "blue"}
alias = colors
print(alias) # {'red', 'green', 'blue'}

# Modify the original set
colors.add("yellow")
print(colors) # {'red', 'green', 'blue', 'yellow'}
# The new variable also changes
print(alias) # {'red', 'green', 'blue', 'yellow'}
```

컴프리헨션으로부터 집합을 만들려면 내부에 수식과 선택적 조건이 있는 중괄호 `{}` 구문을 사용할 수 있다. 이렇게 하면 수식과 조건을 만족하는 요소들로 새로운 집합을 생한다. 예를 들어 다음과 같다.

```python
# Create a set from a comprehension
# A set of even numbers from 0 to 10
evens = {x for x in range(11) if x % 2 == 0}
print(evens) # {0, 2, 4, 6, 8, 10}
```

컴프리헨션는 리스트, 튜플, 문자열, range 또는 다른 집합과 같이 기존에 반복할 수 있는 객체로부터 새로운 집합을 만드는 간결한 방법이다. 유효한 집합 요소를 생성하는 한, 임의의 유효한 표현삭과 조건을 사용할 수 있다.

이 절에서는 숫자, 문자열, 튜플, 리스트, 사전, 집합 자체와 같은 다양한 데이터 타입에서 집합을 만드는 방법을 배웠다. 또한 기존의 반복 가능한 객체에서 새로운 집합을 만드는 간결한 방법인 컴프리헨션으로부터 집합을 만드는 방법도 배웠다. 다음 절에서는 인덱싱과 루프를 사용하여 집합 내 요소에 액세스하는 방법을 설명할 것이다.

## <a name="sec_04"></a> 집합에서 요소 액세스 방법
[앞 절](#sec_03)에서는 숫자, 문자열, 튜플, 리스트, 사전, 집합 자체와 같은 다양한 데이터 타입으로부터 집합을 만드는 방법을 배웠다. 또한 기존의 반복 가능한 객체에서 새로운 집합을 만드는 간결한 방법인 컴프리헨션으로부터 집합을 만드는 방법도 배웠다. 이 절에서는 인덱싱과 루프를 사용하여 집합 내 요소를 액세스하는 방법을 설명할 것이다.

리스트, 튜플, 문자열과 같은 다른 데이터 타입과 달리 집합은 순서가 지정되지 않은 요소 집합이다. 이는 집합의 요소가 고정된 위치나 순서를 가지고 있지 않다는 것을 의미한다. 따라서 인덱싱 또는 슬라이싱을 사용하여 집합의 요소에 접근할 수 없다. 이러한 작업은 요소의 특정 순서를 필요로 하기 때문이다. 예를 들어 인덱스[0]를 사용하여 집합의 첫 번째 요소에 접근하려고 하면 다음과 같은 오류가 발생한다.

```python
# Try to access the first element of a set using indexing
fruits = {"apple", "banana", "cherry"}
print(fruits[0]) # TypeError: 'set' object is not subscriptable
```

집합의 요소에 접근하는 유일한 방법은 `for` 루프를 사용하여 집합에서 반복하고 각 요소를 하나씩 액세스하는 것이다. `for` 루프는 반복 가능한 객체의 각 요소에 대해 리스트, 튜플, 문자열, range 또는 집합과 같은 코드 블록을 반복적으로 실행할 수 있도록 하는 제어 구조이다. `for` 루프의 구문은 다음과 같다.

```python
# Syntax of a for loop
for element in iterable_object:
    # Do something with element
```

여기서 `element`는 반복가능한 객체의 현재 요소를 유지하는 변수이고, `iterable_object`는 반복가능한 객체이다. `for` 루프 내부의 코드 블록은 반복가능한 객체의 각 요소에 대해 들여쓰기되고 실행된다. 예를 들어 집합의 각 요소를 인쇄하기 위해 다음과 같은 `for` 루프를 사용할 수 있다.

```python
# Print each element of a set using a for loop
fruits = {"apple", "banana", "cherry"}
for fruit in fruits:
    print(fruit)
# Output:
# apple
# banana
# cherry
```

집합은 요소들의 순서가 정해지지 않은 집합이므로 출력되는 요소들의 순서는 달라질 수 있다. 또한 현재 요소를 유지하는 변수는 Python에서 유효한 식별자라면 어떤 이름이든 사용할 수 있다.

이 절에서 `for` 루프를 사용하여 집합 내 요소를 액세스하는 방법을 배웠다. 집합은 요소의 순서가 정해지지 않은 집합이므로 인덱싱 또는 슬라이싱을 사용하여 집합 내 요소를 액세스할 수 없다는 것도 배웠다. [다음 절](#sec_05)에서 메서드와 연산자를 사용하여 집합 내 요소를 추가하고 제거하는 방법을 설명할 것이다.

## <a name="sec_05"></a> 집합에서 요소 추가와 제거 방법
[앞 절](#sec_04)에서 `for` 루프를 사용하여 집합 내 요소들을 액세스하는 방법을 배웠다. 집합은 요소들의 순서가 정해지지 않은 집합이기 때문에 인덱싱 또는 슬라이싱을 사용하여 집합 내 요소들에 액세스할 수 없다는 사실도 배웠다. 이 절에서는 에서드와 연산자를 사용하여 집합 내 원소들을 추가하고 제거하는 방법을 설명할 것이다.

집합에 단일 요소를 추가하려면 집합의 `add()` 메서드를 사용할 수 있다. `add()` 메서드는 집합에 추가하려는 요소인 인수 하나를 사용한다. `add()` 메서드는 아무것도 반환하지 않고 원래 집합을 수정한다. 예를 들면 다음과 같다.

```python
# Add a single element to a set using add() method
fruits = {"apple", "banana", "cherry"}
fruits.add("orange")
print(fruits) # {'apple', 'banana', 'cherry', 'orange'}
```

추가하려는 요소가 이미 집합에 있는 경우 집합에는 고유한 요소만 포함되므로 `add()` 메서드는 효과가 없다. 예를 들면 다음과 같다.

```python
# Add an existing element to a set using add() method
fruits = {"apple", "banana", "cherry"}
fruits.add("apple")
print(fruits) # {'apple', 'banana', 'cherry'}
```

집합에 여러 요소를 추가하려면 집합의 `update()` 메서드를 사용할 수 있다. `update()` 메서드는 리스트, 튜플, 문자열, range 또는 다른 집합과 같은 반복 가능한 하나 이상의 객체를 가져와 해당 요소를 집합에 추가한다. `update()` 메서드는 아무것도 반환하지 않고 원래 집합을 수정한다. 예를 들면 다음과 같다.

```python
# Add multiple elements to a set using update() method
fruits = {"apple", "banana", "cherry"}
fruits.update(["mango", "grape", "watermelon"])
print(fruits) # {'apple', 'banana', 'cherry', 'mango', 'grape', 'watermelon'}
```

집합에는 고유한 요소만 포함되므로 반복 가능한 개체에서 중복되는 요소는 무시된다. 예를 들면 다음과 같다.

```python
# Add duplicate elements to a set using update() method
fruits = {"apple", "banana", "cherry"}
fruits.update(["apple", "banana", "orange"])
print(fruits) # {'apple', 'banana', 'cherry', 'orange'}
```

집합에서 단일 원소를 제거하려면 집합의 `remove()` 메서드 또는 `discard()` 메서드를 사용할 수 있다. 두 메서드 모두 하나의 인수를 취하는데, 이 인수는 집합에서 제거하려는 요소이다. 두 메서드의 차이점은 요소가 집합에 없으면 `remove()` 메서드는 오류를 발생시키며, 반면 `discard()` 메서드는 아무런 동작도 하지 않는다는 것이다. 두 메서드 모두 아무것도 반환하지 않고 원래 집합을 수정한다. 예를 들면 다음과 같다.

```python
# Remove a single element from a set using remove() method
fruits = {"apple", "banana", "cherry"}
fruits.remove("banana")
print(fruits) # {'apple', 'cherry'}

# Remove a non-existing element from a set using remove() method
fruits = {"apple", "banana", "cherry"}
fruits.remove("orange") # KeyError: 'orange'

# Remove a single element from a set using discard() method
fruits = {"apple", "banana", "cherry"}
fruits.discard("banana")
print(fruits) # {'apple', 'cherry'}

# Remove a non-existing element from a set using discard() method
fruits = {"apple", "banana", "cherry"}
fruits.discard("orange")
print(fruits) # {'apple', 'banana', 'cherry'}
```

집합에서 모든 원소를 제거하려면 집합의 `clear()` 메서드를 사용할 수 있다. `clear()` 메서드는 인수를 사용하지 않고 집합에서 모든 원소를 제거하여 비운다. `clear()` 메서드는 아무것도 반환하지 않고 원래 집합을 수정한다. 예를 들면 다음과 같다.

```python
# Remove all elements from a set using clear() method
fruits = {"apple", "banana", "cherry"}
fruits.clear()
print(fruits) # set()
```

이 절에서는 메소드와 연산자를 사용하여 집합 내의 원소를 더하고 제거하는 방법을 배웠다. `remove()` 메소드와 `discard()` 메소드의 차이점, 그리고 `clear()` 메소드를 사용하여 집합을 비우는 방법도 배웠다. [다음 절](#sec_06)에서는 `for` 루프를 사용하여 집합에서 반복하는 방법을 설명할 것이다.

## <a name="sec_06"></a> 집합으로 반복하는 방법
[앞 절](#sec_05)에서 메소드와 연산자를 사용하여 집합 내의 원소를 더하고 제거하는 방법을 살펴 보았다. 또한 `remove()` 메소드와 `discard()` 메소드의 차이점, 그리고 `clear()` 메소드를 사용하여 집합을 비우는 방법도 살펴 보았다. 이 절에서는 `for` 루프를 사용하여 집합에서 반복하는 방법을 설명할 것이다.

이미 알고 있듯이 `for` 루프는 반복 가능한 객체의 각 요소에 대해 리스트, 튜플, 문자열, range 또는 집합과 같은 코드 블록을 반복적으로 실행할 수 있는 제어 구조이다. `for` 루프의 구문은 다음과 같다.

```python
# Syntax of a for loop
for element in iterable_object:
    # Do something with element
```

집합에서 반복하려면 집합을 반복 가능한 객체로 하는 `for` 루프를 사용할 수 있다. 이렇게 하면 집합의 각 요소에 대한 코드 블록이 특별한 순서 없이 실행된다. 예를 들어 집합의 각 요소를 인쇄하려면 다음과 같은 `for` 루프를 사용할 수 있다.

```python
# Print each element of a set using a for loop
fruits = {"apple", "banana", "cherry"}
for fruit in fruits:
    print(fruit)
# Output:
# apple
# banana
# cherry
```

집합은 요소들의 순서가 정해지지 않은 집합이므로 출력되는 요소들의 순서는 달라질 수 있다. 또한 현재 요소를 유지하는 변수는 Python에서 유효한 식별자라면 어떤 이름이든 사용할 수 있다.

`for` 루프 안에서 현재의 요소를 인쇄, 할당, 비교, 함수 호출 등의 어떤 연산이나 계산을 수행할 수 있다. 예를 들어 집합의 각 요소가 palindrome인지 확인하려면 다음과 같은 `for` 루프를 사용하면 된다.

```python
# Check if each element of a set is a palindrome using a for loop
words = {"racecar", "level", "kayak", "hello", "world"}
for word in words:
    # Reverse the word
    reversed_word = word[::-1]
    # Check if the word is equal to its reverse
    if word == reversed_word:
        print(word, "is a palindrome")
    else:
        print(word, "is not a palindrome")
# Output:
# racecar is a palindrome
# level is a palindrome
# kayak is a palindrome
# hello is not a palindrome
# world is not a palindrome
```

이 절에서 `for` 루프를 이용하여 집합에서 반복하는 방법을 배웠다. `for` 루프 안에서 현재의 요소와 함께 어떤 연산이나 계산을 수행하는 방법도 배웠다. [다음 절](#sec_07)에서 집합의 메서드와 연산자를 이용하여 합집합, 교집합, 차집합, 대칭 차집합 등의 연산을 수행하는 방법을 살펴 볼 것이다.

## <a name="sec_07"></a> 집합 메소드와 연산자 사용법
[앞 절](#sec_06)에서 `for` 루프를 사용하여 집합에서 반복하는 방법을 배웠다. `for` 루프 안에서 현재 요소와 함께 어떤 연산이나 계산을 수행하는 방법도 배웠다. 이 절에서 집합의 메서드와 연산자를 사용하여 합집합, 교집합, 차집합, 대칭 차집합 등의 연산을 수행하는 방법을 살펴볼 것이다.

집합 메서드는 집합 객체에 속하는 함수로 점 표기법을 사용하여 호출할 수 있다. 예를 들어 `add()` 메서드는 `set_object.add(element)`와 같이 집합 객체에서 호출할 수 있는 집합 메서드이다. 집합 메서드는 0 이상의 인수를 취할 수 있으며 값을 반환하거나 원래 집합을 수정할 수 있다. 예를 들어 `add()` 메서드는 집합에 추가할 요소인 하나의 인수를 취하고 아무것도 반환하지 않으며 원래 집합을 수정한다.

집합 연산자는 연산을 수행하기 위해 두 집합 객체 사이에 사용할 수 있는 기호이다. 예를 들어 `|` 연산자는 `set_object1 | set_object2` 같이 두 집합 객체 사이에 사용할 수 있는 집합 연산자이다. 집합 연산자는 연산에 따라 새로운 집합 객체를 반환하거나 부울 값을 반환할 수 있다. 예를 들어 `|` 연산자는 두 집합의 합인 새로운 집합 객체를 반환한다.

가장 일반적인 집합 방법과 연산자는 다음과 같다.

- **`union()` 메서드와 `|` 연산자**: 두 집합의 모든 요소를 포함하는 새 집합을 반환한다. 예를 들면 다음과 같다.

```python
# Use union() method to get the union of two sets
fruits = {"apple", "banana", "cherry"}
colors = {"red", "green", "blue"}
union1 = fruits.union(colors)
print(union1) # {'apple', 'banana', 'cherry', 'red', 'green', 'blue'}

# Use | operator to get the union of two sets
union2 = fruits | colors
print(union2) # {'apple', 'banana', 'cherry', 'red', 'green', 'blue'}
```

- **`interestion()` 메서드와 `&` 연산자**: 두 집합에 공통인 요소만 포함하는 새 집합을 반환한다. 예를 들면 다음과 같다.

```python
# Use intersection() method to get the intersection of two sets
fruits = {"apple", "banana", "cherry", "orange"}
citrus = {"orange", "lemon", "grapefruit"}
intersection1 = fruits.intersection(citrus)
print(intersection1) # {'orange'}

# Use & operator to get the intersection of two sets
intersection2 = fruits & citrus
print(intersection2) # {'orange'}
```

- **`difference()` 메서드와 `—` 연산자**: 첫 번째 집합에는 있지만 두 번째 집합에는 없는 요소만 포함하는 새 집합을 반환한다. 예를 들면 다음과 같다.

```python
# Use difference() method to get the difference of two sets
fruits = {"apple", "banana", "cherry", "orange"}
citrus = {"orange", "lemon", "grapefruit"}
difference1 = fruits.difference(citrus)
print(difference1) # {'apple', 'banana', 'cherry'}

# Use - operator to get the difference of two sets
difference2 = fruits - citrus
print(difference2) # {'apple', 'banana', 'cherry'}
```

- **`symmetric_difference()` 메서드와 `^` 연산자**: 두 집합에 모두 있는 원소는 제외하고 오작 한 집합에만 있는 요소들로 이루어진 새 집합을 반환한다. 예를 들면 다음과 같다.

```python
# Use symmetric_difference() method to get the symmetric difference of two sets
fruits = {"apple", "banana", "cherry", "orange"}
citrus = {"orange", "lemon", "grapefruit"}
symmetric_difference1 = fruits.symmetric_difference(citrus)
print(symmetric_difference1) # {'apple', 'banana', 'cherry', 'lemon', 'grapefruit'}

# Use ^ operator to get the symmetric difference of two sets
symmetric_difference2 = fruits ^ citrus
print(symmetric_difference2) # {'apple', 'banana', 'cherry', 'lemon', 'grapefruit'}
```

- **`issubset ()` 메서드와 `<=` 연산자**: 첫 번째 집합이 두 번째 집합의 부분 집합인지 여부를 나타내는 부울 값을 반환한다. 이는 첫 번째 집합의 모든 요소가 두 번째 집합에도 있음을 의미한다. 예를 들면,

```python
# Use issubset() method to check if a set is a subset of another set
fruits = {"apple", "banana", "cherry"}
basket = {"apple", "banana", "cherry", "orange", "mango"}
issubset1 = fruits.issubset(basket)
print(issubset1) # True

# Use <= operator to check if a set is a subset of another set
issubset2 = fruits <= basket
print(issubset2) # True
```

- **`issuperset()` 메서드와 `>=` 연산자**: 첫 번째 집합이 두 번째 집합의 superset인지 여부를 나타내는 부울 값을 반환하며, 이는 두 번째 집합의 모든 요소가 첫 번째 집합에 있음을 의미한다. 예를 들면,

```python
# Use issuperset() method to check if a set is a superset of another set
fruits = {"apple", "banana", "cherry"}
basket = {"apple", "banana", "cherry", "orange", "mango"}
issuperset1 = basket.issuperset(fruits)
print(issuperset1) # True

# Use >= operator to check if a set is a superset of another set
issuperset2 = basket >= fruits
print(issuperset2) # True
```

- **`isdisjoint()` 메서드**: 두 집합에 공통 요소가 없는지 여부를 나타내는 부울 값을 반환한다. 예를 들면 다음과 같다.

```python
# Use isdisjoint() method to check if two sets are disjoint
fruits = {"apple", "banana", "cherry"}
colors = {"red", "green", "blue"}
isdisjoint = fruits.isdisjoint(colors)
print(isdisjoint) # True
```

이 절에서는 집합의 메서드와 연산자를 이용하여 합집합, 교집합, 차집합, 대칭 차집합 등의 연산을 수행하는 방법을 배웠다. 집합의 메서드와 연산자를 이용하여 subset, superset, disjoint를 이용하여 집합을 비교하는 방법도 배웠다. [다음 절](#sec_08)에서 크기, 등분, 멤버십 등 다른 기준을 이용하여 집합을 비교하는 방법을 다룰 것이다.

## <a name="sec_08"></a> 집합 비교 방법
[앞 절](#sec_07)에서 집합의 메서드와 연산자를 이용하여 합집합, 교집합, 차집합, 대칭 차집합 등의 연산을 수행하는 방법을 배웠다. 또한 집합의 메서드와 연산자를 이용하여 subset, superset, disjoint를 이용하여 집합을 비교하는 방법도 배웠다. 이 절에서 크기, 등분, 멤버십 등 다른 기준을 이용하여 집합을 비교하는 방법을 다룰 것이다.

두 집합의 크기를 비교하기 위해서는 한 집합의 원소 개수를 반환하는 `len()` 함수를 사용하면 된다. 예를 들어 어떤 집합이 더 큰지 확인하기 위해서는 `len()` 함수를 다음과 같이 사용하면 된다.

```python
# Compare the size of two sets using len() function
fruits = {"apple", "banana", "cherry"}
colors = {"red", "green", "blue", "yellow"}
if len(fruits) > len(colors):
    print("The fruits set is larger")
elif len(fruits) < len(colors):
    print("The colors set is larger")
else:
    print("The sets are equal in size")
# Output:
# The colors set is larger
```

두 집합의 같음을 비교하기 위해 두 집합의 원소가 같으면 True, 그렇지 않으면 False를 반환하는 `==` 연산자를 사용할 수 있다. 예를 들어 두 집합이 동일한지 확인하기 위해 다음과 같은 `==` 연산자를 사용할 수 있다.

```python
# Compare the equality of two sets using == operator
fruits1 = {"apple", "banana", "cherry"}
fruits2 = {"cherry", "banana", "apple"}
fruits3 = {"apple", "orange", "grape"}
print(fruits1 == fruits2) # True
print(fruits1 == fruits3) # False
```

집합은 원소들의 순서가 정해지지 않은 집합이므로 집합들의 원소들의 순서는 중요하지 않다. 또한 `!=` 연산자는 `==` 연산자의 반대를 반환하므로 두 집합이 같지 않으면 True, 그렇지 않으면 False를 반환한다.

집합에 있는 원소의 구성원을 비교하기 위해서는 원소가 집합에 있으면 True, 그렇지 않으면 False를 반환하는 `in` 연산자를 사용할 수 있다. 예를 들어 원소가 집합에 있는지 확인하기 위해서는 다음과 같은 `in` 연산자를 사용할 수 있다.

```python
# Compare the membership of an element in a set using in operator
fruits = {"apple", "banana", "cherry"}
print("apple" in fruits) # True
print("orange" in fruits) # False
```

`not in` 연산자는 `in` 연산자의 반대를 반환하므로 요소가 집합에 없으면 True, 그렇지 않으면 False를 반환한다.

이 절에서, 우리는 크기, 등식, 멤버쉽 등과 같은 다른 기준을 사용하여 집합을 비교하는 방법을 배웠다. 또한 `len ()` 함수, `==` 및 `!=` 연산자, `in`과 `not in` 연산자를 사용하여 집합을 비교하는 방법도 배웠다. [다음 절](#sec_09)에서, 집합을 리스트, 튜플, 사전과 같은 다른 자료 타입으로 변환하는 방법을 설명할 것이다.

## <a name="sec_09"></a> 집합에서 다른 데이터 타입으로 변환 방법
[앞 절](#sec_08)에서, 크기, 등식, 멤버쉽 등과 같은 다른 기준을 사용하여 집합을 비교하는 방법을 배웠다. 또한 `len ()` 함수, `==` 및 `!=` 연산자, `in`과 `not in` 연산자를 사용하여 집합을 비교하는 방법도 배웠다. 집합을 리스트, 튜플, 사전과 같은 다른 자료 타입으로 변환하는 방법을 설명할 것이다.

집합을 리스트로 변환하려면 `list()` 함수를 사용하면 되는데, `list()` 함수는 집합과 같이 반복 가능한 객체를 취하여 객체의 요소를 포함하는 새로운 목록을 반환한다. 예를 들어 집합을 리스트로 변환하려면 `list()` 함수를 다음과 같이 사용하면 된다.

```python
# Convert a set to a list using list() function
fruits = {"apple", "banana", "cherry"}
fruits_list = list(fruits)
print(fruits_list) # ['apple', 'banana', 'cherry']
```

집합은 요소들의 순서가 정해지지 않기 때문에, 목록에 있는 요소들의 순서는 달라질 수 있다. 또한, 원래 집합은 수정되지 않고, 새로운 목록만 생성된다.

집합을 튜플로 변환하려면 집합과 같이 반복 가능한 객체를 취하고 객체의 요소를 포함하는 새로운 튜플을 반환하는 `tuple()` 함수를 사용할 수 있다. 예를 들어 집합을 튜플로 변환하려면 `tuple()` 함수를 다음과 같이 사용할 수 있다.

```python
# Convert a set to a tuple using tuple() function
fruits = {"apple", "banana", "cherry"}
fruits_tuple = tuple(fruits)
print(fruits_tuple) # ('apple', 'banana', 'cherry')
```

집합은 요소들의 순서가 정해지지 않은 집합들이기 때문에 튜플의 요소들의 순서는 달라질 수 있다. 또한, 원래 집합은 수정되지 않고, 새로운 튜플만 생성된다.

집합을 사전으로 변환하기 위해서는 집합과 같은 기존의 반복 가능한 객체로부터 새로운 사전을 생성하는 간결한 방법인 컴프리헨션을 사용할 수 있다. 컴프리헨션의 구문은 다음과 같다.

```python
# Syntax of a comprehension
{key: value for element in iterable_object}
```

집합을 사전으로 변환하려면 집합의 각 요소에 대한 키와 값을 지정해야 한다. 키와 값은 유효한 사전 키와 값을 생성하는 임의의 유효한 표현식일 수 있다. 예를 들어 집합을 사전으로 변환하려면 다음과 같은 컴프리헨션을 사용할 수 있다.

```python
# Convert a set to a dictionary using a comprehension
fruits = {"apple", "banana", "cherry"}
fruits_dict = {fruit: len(fruit) for fruit in fruits}
print(fruits_dict) # {'apple': 5, 'banana': 6, 'cherry': 6}
```

이 컴프리헨션은 집합의 요소들을 키로 하고 그 길이를 값으로 하는 새로운 사전을 만든다. 집합이 요소들의 순서가 정해지지 않은 컬렉션이므로 사전의 요소들의 순서는 달라질 수 있다. 또한 원래 집합은 수정되지 않고 새로운 사전만 생성된다.

이 절에서는 집합을 리스트, 튜플, 사전과 같은 다른 데이터 타입으로 변환하는 방법을 배웠다. `list()`, `tuple()` 함수를 사용하는 방법과 집합을 다른 데이터 타입으로 변환하는 컴프리헨션도 배웠다. [다음 절](#conclusion)에서는 포스팅을 마무리한다.


## <a name="conclusion"></a> 맺으며
Python 집합에 대한 이 포스팅의 끝에 도달했다. 이 포스팅에서 다음과 같은 것들을 배웠다.

- 고유한 요소의 순서가 지정되지 않은 컬렉션인 집합의 생성과 액세스
- 메서드와 연산자를 사용하여 집합 내 요소의 추가와 제거
- for 루프를 사용하여 집합에서 반복
- 집합 메서드와 연산자를 사용하여 합집합, 교집합, 차집합, 대칭 차집합 등의 연산 수행
- subset, superset, disjoint 등의 메서드와 연산자를 사용한 집합 비교
- 집합을 목록, 튜플 및 사전과 같은 다른 데이터 타입으로 변환

<a name="footnote_1">1</a>: 수학에서는 집합의 "element"를 "원소"라고 번역한다. 이 튜튜리얼 시리즈에서 다른 데이터 타입과 일관성을 위하여 "요소"로 번역하였다.
