# Python 집합: 생성, 맥세스 및 메소드 <sup>[1](#footnote_1)</sup>

> <font size="3">불변의 컬렉션인 튜플을 만들고 액세스하는 방법에 대해 알아본다.</font>

## 목차

1. [들어가며](./sets.md#intro)
1. [Python에서 집합이란?](./sets.md#sec_02)
1. [Python에서 집합 생성 방법](./sets.md#sec_03)
1. [집합에서 요소 액세스 방법](./sets.md#sec_04)
1. [집합에서 요소 추가와 제거 방법](./sets.md#sec_05)
1. [집합으로 반복하는 방법](./sets.md#sec_06)
1. [집합 메소드와 연산자 사용법](./sets.md#sec_07)
1. [집합 비교 방법](./sets.md#sec_08)
1. [집합에서 다른 데이터 타입으로 변환 방법](./sets.md#sec_09)
1. [맺으며](./sets.md#conclusion)


<a name="footnote_1">1</a>: [Python Tutorial 11 — Python Sets: Creation, Access, and Methods](https://python.plainenglish.io/python-tutorial-11-python-sets-creation-access-and-methods-e025bd072171?sk=8f911897f0c398363e6ba6905df4a1c0)를 편역한 것입니다.
